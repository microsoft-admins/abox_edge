#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Select the SoC
$(call set_soc, marvell, iap140)

# Add wifi controlller
$(call add_peripheral, marvell, wifi/sd8xxx)

# Enable ext4 image building if the flash type is emmc
TARGET_USERIMAGES_USE_EXT4 := true

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1073741824
BOARD_USERDATAIMAGE_PARTITION_SIZE := 4343595008
BOARD_CACHEIMAGE_PARTITION_SIZE := 268435456

BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 512

TARGET_BOARD_INFO_FILE := device/marvell/abox_edge/board-info.txt

PRODUCT_COPY_FILES += \
    device/marvell/abox_edge/fstab:root/fstab.${soc_name} \
    device/marvell/abox_edge/provision-device:provision-device

# Add media codec support
PRODUCT_COPY_FILES += \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs.xml

vendor_partition_directory := vendor/bsp/marvell/device/abox_edge
BRILLO_VENDOR_PARTITIONS := \
    $(vendor_partition_directory):bin/primary_gpt \
    $(vendor_partition_directory):bin/misc.bin \
    $(vendor_partition_directory):bootloader/u-boot.bin \
    $(vendor_partition_directory):bootloader/teesst.img

# Must be defined at the end of the file
$(call add_device_packages)
